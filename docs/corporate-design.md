# Coporate design

## Colors

| Name      | Value   |
|:----------|:--------|
| Primary   | #004455 |
| Secondary | #444444 |
| Dark      | #111111 |

## Fonts

| Name      | Value                    |
|:----------|:-------------------------|
| Headlines | Montserat                |
| Body copy | 'Open Sans', sans-serif; |
| Print     | Calibri                  |


## Logo

| Name                   | Url                                       |                                                                   |
|:-----------------------|:------------------------------------------|:------------------------------------------------------------------|
| Logo Dark              | `https://s3.buckpesch.io/images/logo.png` | ![Logo dark](https://s3.buckpesch.io/images/logo.png "Logo dark") |
| Logo transparent white | #444444                                   |                                                                   |
| Logo transparent dark  | #111111                                   |                                                                   |




## Slogan

- "Cloud & IT"