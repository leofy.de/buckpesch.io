<?php

namespace App\Controllers;

use App\Models\Medium\Post;
use App\Models\Medium\Reader;
use App\Models\WordpressReader;

class HomeController extends AbstractController
{
  /** @var  $activatedSections array create a getBlog(), getSkills(), ... method to return data */
  private $activatedSections = [
    'header',
    'blog',
    'about',
    'skills',
    'education',
    'work',
    'promo'
    //'contact',
  ];

  /**
   * Renders the home page
   * @return \Psr\Http\Message\ResponseInterface|\Slim\Http\Response
   */
  public function get()
  {
    global $am;
    $sections = [];

    // Remove sections, which are not activated
    $activatedSections = $this->activatedSections;
    if ($am->getConfig('blog_posts_max') === 0) {
      $activatedSections = array_diff($activatedSections, ['blog']);
    }
    if ($am->getConfig('skills_amount') === 0) {
      $activatedSections = array_diff($activatedSections, ['skills']);
    }
    if ($am->getConfig('education_amount') === 0) {
      $activatedSections = array_diff($activatedSections, ['education']);
    }
    $this->activatedSections = array_values($activatedSections);

    // Get data for all activated sections
    $index = 1;
    foreach ($activatedSections as $activatedSection) {
      $function = 'get' . ucwords($activatedSection);
      if (method_exists($this, $function)) {
        $sections[$activatedSection] = $this->$function();
        $sections[$activatedSection]['slug'] = $activatedSection;

        // Set section index and increase counter
        $sections[$activatedSection]['index'] = '0' . $index;
        $index++;
      }
    }

    // Get all activated languages for the site
    $languages = $am->getLanguages();
    $languageCodeCurrent = $am->getLang();
    $languageCurrent = [
      'title' => $am->getTranslation($languageCodeCurrent),
      'image' => '/images/flags/24/' . strtolower(substr($languageCodeCurrent, 3, 2)) . '.png'
    ];
    $languagesActivated = [];
    foreach ($languages['activated'] as $language) {
      $languagesActivated[] = [
        'current' => $language['lang'] === $languageCodeCurrent,
        'title' => $am->getTranslation($language['lang']),
        'image' => '/images/flags/24/' . strtolower(substr($language['lang'], 3, 2)) . '.png',
        'url' => $am->getBaseUrl() . '?lang=' . $language['lang']
      ];
    }
    if (count($languagesActivated) <= 1) {
      $languagesActivated = false;
    }

    return $this->view->render($this->response, 'home.html.twig', [
      'sections' => $sections,
      'navigation' => [
        'languages' => $languagesActivated,
        'languageCurrent' => $languageCurrent
      ],
      'page' => $this->getPage(),
      'footer' => $this->getFooter()
    ]);
  }

  /**
   * Returns general page settings
   * @return array
   */
  private function getPage()
  {
    global $am;

    return [
      'favicon' => $am->getConfig('page_favicon'),
      'logo' => $am->getConfig('page_logo'),
      'title' => $am->getConfig('page_title'),
      'social_media' => [
        'facebook' => $am->getConfig('page_social_media_facebook'),
        'medium' => $am->getConfig('page_social_media_medium'),
        'github' => $am->getConfig('page_social_media_github'),
        'xing' => $am->getConfig('page_social_media_xing'),
        'linkedin' => $am->getConfig('page_social_media_linkedin')
      ],
      'copyright' => date('Y') . ' ' . $am->getConfig('about_name')
    ];
  }

  /**
   * Returns data for about section
   * @return array
   */
  private function getAbout(): array
  {
    global $am;

    // Get all activated languages
    $languages = [];
    for ($i = 1; $i <= 5; $i++) {
      $code = $am->getConfig('about_language_' . $i . '_code');
      if ($code !== 'none') {
        $languages[] = [
          'level' => $am->getConfig('about_language_' . $i . '_level'),
          'title' => $am->getTranslation($code),
          'src' => '/images/flags/24/' . strtolower(substr($code, 3, 2)) . '.png'
        ];
      }
    }

    $data = [
      'title' => $am->getTranslation('about'),
      'name' => $am->getConfig('about_name'),
      'location' => $am->getConfig('about_location'),
      'email' => $am->getConfig('about_email'),
      'image' => $am->getConfig('about_image'),
      'nationality' => $am->getConfig('about_nationality'),
      'phone' => $am->getConfig('about_phone'),
      'slogan' => $am->getConfig('about_slogan'),
      'resume' => $am->getConfig('about_resume'),
      'languages' => $languages
    ];

    return $data;
  }

  /**
   * Returns data for header section
   * @return array
   */
  private function getHeader(): array
  {
    global $am;

    // Prepare slogans
    $slogans = [];
    for ($i = 1; $i <= 6; $i++) {
      $slogan = $am->getConfig('header_slogan_' . $i);
      if (strlen(trim($slogan)) > 0) {
        $slogans[] = $slogan;
      }
    }

    // Get second section for the scroll down effect
    $scrollTo = '#';
    if (is_array($this->activatedSections) && count($this->activatedSections) > 1) {
      $scrollTo = '#' . $this->activatedSections[1];
    }

    return [
      'title' => $am->getTranslation('home'),
      'background' => $am->getConfig('header_background_image'),
      'headline' => $am->getConfig('header_headline'),
      'scrollTo' => $scrollTo,
      'slogans' => $slogans
    ];
  }

  /**
   * Returns data for footer section
   * @return array
   */
  private function getFooter()
  {
    global $am;

    // Render imprint
    $imprint = $this->view->fetch('partials/components/modal.html.twig', [
      'id' => 'imprint',
      'title' => $am->getTranslation('imprint'),
      'body' => $am->getConfig('footer_imprint_content')
    ]);

    return [
      'imprint' => $imprint
    ];
  }

  /**
   * Returns data for contact section
   * @return array
   */
  private function getContact(): array
  {
    global $am;

    return [
      'title' => $am->getTranslation('contact')
    ];
  }

  /**
   * Returns a list of skills
   * @return array Array list of skills
   */
  private function getSkills()
  {
    global $am;

    // Prepare items array
    $items = [];
    $itemsAmount = $am->getConfig('skills_amount');
    for ($i = 1; $i <= $itemsAmount; $i++) {
      $items[] = [
        'icon' => $am->getConfig('skills_' . $i . '_icon'),
        'title' => $am->getConfig('skills_' . $i . '_title'),
        'description' => $am->getConfig('skills_' . $i . '_description')
      ];
    }

    $response = [
      'title' => $am->getTranslation('skills'),
      'items' => $items
    ];

    return $response;
  }

  /**
   * Returns a list of blogs and all related information about recent publtications
   * @return array Array list of skills
   * @throws \Exception
   */
  private function getBlog()
  {
    global $am;
    // Number of posts to be displayed
    $postsLimit = (int) $am->getConfig('blog_posts_max');
    $medium = new Reader($am->getConfig('blog_medium_username'));
    $posts = $medium->getPosts();
    $postsArray = [];
    /** @var Post $post */
    foreach ($posts as $post) {
      $data = $post->toArray();
      $data['previewImage'] = $post->getPreviewImage(600);
      $data['timestamp'] = $post->getPublishedAt() ? $post->getPublishedAt()->getTimestamp() : '';
      $data['publishedAt'] = date('d F Y', $post->getPublishedAt()->getTimestamp());
      $postsArray[] = $data;
    }

    // Get wordpress posts from App-Arena.com
    $wpReader = new WordpressReader($am->getConfig('blog_wordpress_user_id'));
    $wpPosts = $wpReader->getPosts();
    foreach ($wpPosts as $post) {
      $data = $post->toArray();
      $data['previewImage'] = $post->getPreviewImage();
      $data['timestamp'] = $post->getPublishedAt();
      $data['publishedAt'] = date('d F Y', $post->getPublishedAt());
      $postsArray[] = $data;
    }

    // Order posts by date
    $timestamp = [];
    foreach ($postsArray as $key => $row) {
      $timestamp[$key] = $row['timestamp'];
    }
    array_multisort($timestamp, SORT_DESC, $postsArray);

    // Only take the 6 most recent posts
    $postsArray = array_slice($postsArray, 0, $postsLimit);

    $response = [
      'author' => [
        'name' => $am->getConfig('about_name'),
        'image' => $am->getConfig('about_image')
      ],
      'title' => $am->getTranslation('blog'),
      'items' => $postsArray
    ];

    return $response;
  }

  /**
   * Returns a list of education items
   * @return array Array list of education items
   */
  private function getEducation()
  {
    global $am;

    // Prepare items array
    $items = [];
    $itemsAmount = $am->getConfig('education_amount');
    for ($i = 1; $i <= $itemsAmount; $i++) {
      $items[] = [
        'title' => $am->getConfig('education_' . $i . '_title'),
        'description' => $am->getConfig('education_' . $i . '_description'),
        'image' => $am->getConfig('education_' . $i . '_image'),
        'degree' => $am->getConfig('education_' . $i . '_degree'),
        'years' => $am->getConfig('education_' . $i . '_years'),
        'city' => $am->getConfig('education_' . $i . '_city')
      ];
    }

    $response = [
      'title' => $am->getTranslation('education'),
      'items' => $items
    ];

    return $response;
  }

  /**
   * Returns a list of work items
   * @return array Array list of work items
   */
  private function getWork()
  {
    global $am;

    // Prepare items array
    $items = [];
    $itemsAmount = $am->getConfig('work_amount');
    for ($i = 1; $i <= $itemsAmount; $i++) {
      $items[] = [
        'title' => $am->getConfig('work_' . $i . '_title'),
        'description' => $am->getConfig('work_' . $i . '_description'),
        'image' => $am->getConfig('work_' . $i . '_image'),
        'position' => $am->getConfig('work_' . $i . '_position'),
        'years' => $am->getConfig('work_' . $i . '_years'),
        'city' => $am->getConfig('work_' . $i . '_city')
      ];
    }

    $response = [
      'title' => $am->getTranslation('work'),
      'items' => $items
    ];

    return $response;
  }

  /**
   * Returns all data for downlaod
   * @return array Array list of downloads
   */
  private function getPromo()
  {
    global $am;

    // Get Buttons to Github and Store.buckpesch.io
    $response = false;
    if ($am->getConfig('header_promo_activated')) {
      $response = [
        'title' => $am->getTranslation('like_this_page'),
        'github' => [
          'caption' => $am->getTranslation('clone_on_github'),
          'url' => 'https://github.com/sbuckpesch/buckpesch.io'
        ],
        'store' => [
          'caption' => $am->getTranslation('get_hosted_version_for_free'),
          'url' => 'https://store.buckpesch.io/products'
        ]
      ];
    }

    return $response;
  }
}
