<?php
namespace App\Models\Medium;

use FeedIo\Feed\Item;

class Post
{
  private $title;
  private $subtitle;
  private $publishedAt;
  private $previewImage;
  private $meta;
  private $tags = [];
  private $url;

  /**
   * Post constructor.
   *
   * @param array $data Post data as received from Medium json feed
   */
  public function __construct(Item $data)
  {
    // Parse most important data
    $dataArray = $data->toArray();
    $content = $dataArray['elements']['content:encoded'] ?? '';
    $this->title = $data->getTitle();
    $this->publishedAt = $data->getLastModified();
    $this->tags = $dataArray['categories'] ?? [];
    $this->url = $data->getLink();
    preg_match_all('/<img [^>]*src=["|\']([^"|\']+)/i', $content, $matches);
    if (isset($matches[1][0])) {
      $patterns = explode('/', $matches[1][0]);
      $imageId = array_pop($patterns);
    }
    $this->previewImage = $imageId ?? '';

    // Validate the post
    if (str_word_count($content) <= 100) {
      throw new \InvalidArgumentException('Too less words to accept as post');
    }
  }

  /**
   * Returns all available data of the medium post in an array
   * @return array
   */
  public function toArray(): array
  {
    return [
      'title' => $this->getTitle(),
      // 'subtitle' => $this->getSubtitle(),
      'publishedAt' => $this->getPublishedAt(),
      'previewImage' => $this->getPreviewImage(),
      // 'meta' => $this->getMeta(),
      'tags' => $this->getTags(),
      'url' => $this->getUrl()
    ];
  }

  /**
   * @return mixed|null
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * @return null
   */
  public function getSubtitle()
  {
    return $this->subtitle;
  }

  /**
   * @return mixed|null
   */
  public function getPublishedAt(): \DateTime
  {
    return $this->publishedAt;
  }

  /**
   * @param int $width Required image width
   *
   * @return string Image url
   */
  public function getPreviewImage($width = 800)
  {
    $size = $width . '/';

    return 'https://cdn-images-1.medium.com/max/' . $size . $this->previewImage;
  }

  /**
   * @return array
   */
  public function getMeta(): array
  {
    return $this->meta;
  }

  /**
   * @return array
   */
  public function getTags(): array
  {
    return $this->tags;
  }

  /**
   * @return string
   */
  public function getUrl(): string
  {
    return $this->url;
  }
}
