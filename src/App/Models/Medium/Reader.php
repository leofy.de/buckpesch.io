<?php
namespace App\Models\Medium;

use FeedIo\Factory;
use FeedIo\FeedInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Reader
{
  private $user;

  /** @var array of Medium Posts */
  private $posts;

  /**
   * Medium constructor.
   *
   * @param string $user Medium username, e.g. @sbuckpesch
   * @throws \Exception
   */
  public function __construct($user)
  {
    $this->user = $user;

    // Fetch data from medium json feed
    try {
      $feed = $this->fetch($user);
    } catch (\Exception $e) {
      $feed = [];
    }

    $this->setPosts($feed);
  }

  private function fetch($user, $slug = 'latest'): FeedInterface
  {
    $url = sprintf('https://medium.com/feed/@%s?page=2', $user);

    $feedIo = Factory::create()->getFeedIo();
    $result = $feedIo->read($url);

    return $result->getFeed();
  }

  /**
   * @return array
   */
  public function getPosts(): array
  {
    $timestamp = [];
    foreach ($this->posts as $key => $row) {
      $timestamp[$key] = $row->getPublishedAt();
    }
    array_multisort($timestamp, SORT_DESC, $this->posts);

    return $this->posts;
  }

  /**
   * @param FeedInterface $feed
   */
  public function setPosts(FeedInterface $feed)
  {
    $mediumPosts = [];

    foreach ($feed as $post) {
      try {
        $post = new Post($post);
        $mediumPosts[] = $post;
      } catch (\Exception $e) {
        // Do nothing here as an error occured parsing the post
      }
    }

    $this->posts = $mediumPosts;
  }
}
