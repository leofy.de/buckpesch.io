<?php

/* partials/sections/partials/title.html.twig */
class __TwigTemplate_8936221cbf85eb93e03c6710499ff23072d1bf855e44aef35e77b59fdef89028 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- section title -->
<div class=\"row mt-5 pt-5\">
    <div class=\"col-md-12 text-center\">
        <span class=\"title-number mb-2\">";
        // line 4
        echo twig_escape_filter($this->env, ($context["index"] ?? null), "html", null, true);
        echo "</span>
        <h3 class=\"section-title p-0\">";
        // line 5
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "</h3>
    </div>
</div>
<!-- end section title -->";
    }

    public function getTemplateName()
    {
        return "partials/sections/partials/title.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 5,  24 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- section title -->
<div class=\"row mt-5 pt-5\">
    <div class=\"col-md-12 text-center\">
        <span class=\"title-number mb-2\">{{ index }}</span>
        <h3 class=\"section-title p-0\">{{ title }}</h3>
    </div>
</div>
<!-- end section title -->", "partials/sections/partials/title.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\sections\\partials\\title.html.twig");
    }
}
