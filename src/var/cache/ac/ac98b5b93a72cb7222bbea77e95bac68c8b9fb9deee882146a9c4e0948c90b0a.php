<?php

/* partials/sections/about.html.twig */
class __TwigTemplate_bf2c41d2c58dcaa4568ee62724235118c0471d90a2a869ded866d4975573e28d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- about us section -->
<section id=\"";
        // line 2
        echo twig_escape_filter($this->env, ($context["slug"] ?? null), "html", null, true);
        echo "\" class=\"border-bottom no-padding-bottom xs-onepage-section\">
    <div class=\"container\">

        ";
        // line 5
        $this->loadTemplate("partials/sections/partials/title.html.twig", "partials/sections/about.html.twig", 5)->display($context);
        // line 6
        echo "
        <div class=\"row justify-content-center mt-5 mb-0\">
            <div class=\"col-6 col-sm-4\">
                <img src=\"";
        // line 9
        echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "\"/>
            </div>
            <div class=\"col-sm-8 sm-margin-bottom-ten\">
                <div class=\"no-padding\">
                    <p class=\"text-large\">";
        // line 13
        echo twig_escape_filter($this->env, ($context["slogan"] ?? null), "html", null, true);
        echo "</p>
                    <ul class=\"list-line margin-five text-med\">
                        <li><span class=\"font-weight-600\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute(($context["translations"] ?? null), "name", array()), "html", null, true);
        echo ":</span> ";
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "</li>
                        <li><span class=\"font-weight-600\">";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute(($context["translations"] ?? null), "email", array()), "html", null, true);
        echo ":</span> ";
        echo twig_escape_filter($this->env, ($context["email"] ?? null), "html", null, true);
        echo "</li>
                        <li><span class=\"font-weight-600\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["translations"] ?? null), "phone", array()), "html", null, true);
        echo ":</span> ";
        echo twig_escape_filter($this->env, ($context["phone"] ?? null), "html", null, true);
        echo "</li>
                        <li><span class=\"font-weight-600\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["translations"] ?? null), "nationality", array()), "html", null, true);
        echo ":</span> ";
        echo twig_escape_filter($this->env, ($context["nationality"] ?? null), "html", null, true);
        echo "</li>
                        <li>
                            <span class=\"font-weight-600\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["translations"] ?? null), "languages", array()), "html", null, true);
        echo ":</span>
                            ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 22
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["language"], "src", array()), "html", null, true);
            echo "\"
                                     data-toggle=\"tooltip\"
                                     data-placement=\"top\"
                                     alt=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["language"], "title", array()), "html", null, true);
            echo "\"
                                     title=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["language"], "title", array()), "html", null, true);
            echo " | ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["language"], "level", array()), "html", null, true);
            echo "\">
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "                        </li>
                    </ul>
                    <!--<a class=\"highlight-button-dark btn btn-medium button\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, ($context["resume"] ?? null), "html", null, true);
        echo "\" target=\"_blank\">
                        Download Resume
                    </a>-->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end about us section -->";
    }

    public function getTemplateName()
    {
        return "partials/sections/about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 30,  103 => 28,  93 => 26,  89 => 25,  82 => 22,  78 => 21,  74 => 20,  67 => 18,  61 => 17,  55 => 16,  49 => 15,  44 => 13,  35 => 9,  30 => 6,  28 => 5,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- about us section -->
<section id=\"{{ slug }}\" class=\"border-bottom no-padding-bottom xs-onepage-section\">
    <div class=\"container\">

        {% include 'partials/sections/partials/title.html.twig' %}

        <div class=\"row justify-content-center mt-5 mb-0\">
            <div class=\"col-6 col-sm-4\">
                <img src=\"{{ image }}\" alt=\"{{ name }}\"/>
            </div>
            <div class=\"col-sm-8 sm-margin-bottom-ten\">
                <div class=\"no-padding\">
                    <p class=\"text-large\">{{ slogan }}</p>
                    <ul class=\"list-line margin-five text-med\">
                        <li><span class=\"font-weight-600\">{{ translations.name }}:</span> {{ name }}</li>
                        <li><span class=\"font-weight-600\">{{ translations.email }}:</span> {{ email }}</li>
                        <li><span class=\"font-weight-600\">{{ translations.phone }}:</span> {{ phone }}</li>
                        <li><span class=\"font-weight-600\">{{ translations.nationality }}:</span> {{ nationality }}</li>
                        <li>
                            <span class=\"font-weight-600\">{{ translations.languages }}:</span>
                            {% for language in languages %}
                                <img src=\"{{ language.src }}\"
                                     data-toggle=\"tooltip\"
                                     data-placement=\"top\"
                                     alt=\"{{ language.title }}\"
                                     title=\"{{ language.title }} | {{ language.level }}\">
                            {% endfor %}
                        </li>
                    </ul>
                    <!--<a class=\"highlight-button-dark btn btn-medium button\" href=\"{{ resume }}\" target=\"_blank\">
                        Download Resume
                    </a>-->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end about us section -->", "partials/sections/about.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\sections\\about.html.twig");
    }
}
