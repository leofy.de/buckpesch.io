<?php

/* partials/sections/blog.html.twig */
class __TwigTemplate_2662afbb9534445dbf187eed0016b22fa708a6f24d6081e6be4f6f0128393d79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- blog section -->
<section id=\"";
        // line 2
        echo twig_escape_filter($this->env, ($context["slug"] ?? null), "html", null, true);
        echo "\" class=\"xs-onepage-section\">
    <div class=\"container\">

        ";
        // line 5
        $this->loadTemplate("partials/sections/partials/title.html.twig", "partials/sections/blog.html.twig", 5)->display($context);
        // line 6
        echo "
        <div class=\"row mt-3 mb-0\">
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 9
            echo "                <!-- blog item -->
                <div class=\"col-md-4 col-sm-4 wow fadeInUp mt-3\">
                    <div class=\"blog-post\">
                        <div class=\"blog-post-images d-flex align-items-center\">
                            <a href=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\" target=\"_blank\">
                                <img src=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "previewImage", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "\">
                            </a>
                        </div>
                        <div class=\"post-details\">
                            <h4 class=\"my-2\">
                                <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "url", array()), "html", null, true);
            echo "\" target=\"_blank\" class=\"post-title\">
                                    ";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "
                                </a>
                            </h4>
                            <div class=\"post-author media\">
                                <img class=\"mr-2 rounded-circle\" src=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute(($context["author"] ?? null), "image", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["author"] ?? null), "name", array()), "html", null, true);
            echo "\">
                                <div class=\"media-body\">
                                    <h5 class=\"mb-1\">";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute(($context["author"] ?? null), "name", array()), "html", null, true);
            echo "</h5>
                                    <p class=\"text-muted\">";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "publishedAt", array()), "html", null, true);
            echo "</p>
                                </div>
                            </div>
                            ";
            // line 31
            echo "                        </div>
                    </div>
                </div>
                <!-- end blog item -->
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "        </div>
    </div>
</section>
<!-- end blog section -->";
    }

    public function getTemplateName()
    {
        return "partials/sections/blog.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 36,  86 => 31,  80 => 27,  76 => 26,  69 => 24,  62 => 20,  58 => 19,  48 => 14,  44 => 13,  38 => 9,  34 => 8,  30 => 6,  28 => 5,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- blog section -->
<section id=\"{{ slug }}\" class=\"xs-onepage-section\">
    <div class=\"container\">

        {% include 'partials/sections/partials/title.html.twig' %}

        <div class=\"row mt-3 mb-0\">
            {% for post in items %}
                <!-- blog item -->
                <div class=\"col-md-4 col-sm-4 wow fadeInUp mt-3\">
                    <div class=\"blog-post\">
                        <div class=\"blog-post-images d-flex align-items-center\">
                            <a href=\"{{ post.url }}\" target=\"_blank\">
                                <img src=\"{{ post.previewImage }}\" alt=\"{{ post.title }}\">
                            </a>
                        </div>
                        <div class=\"post-details\">
                            <h4 class=\"my-2\">
                                <a href=\"{{ post.url }}\" target=\"_blank\" class=\"post-title\">
                                    {{ post.title }}
                                </a>
                            </h4>
                            <div class=\"post-author media\">
                                <img class=\"mr-2 rounded-circle\" src=\"{{ author.image }}\" alt=\"{{ author.name}}\">
                                <div class=\"media-body\">
                                    <h5 class=\"mb-1\">{{ author.name }}</h5>
                                    <p class=\"text-muted\">{{ post.publishedAt }}</p>
                                </div>
                            </div>
                            {#<p>{{ post.subtitle|raw }}</p>#}
                        </div>
                    </div>
                </div>
                <!-- end blog item -->
            {% endfor %}
        </div>
    </div>
</section>
<!-- end blog section -->", "partials/sections/blog.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\sections\\blog.html.twig");
    }
}
