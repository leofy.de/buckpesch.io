<?php

/* partials/sections/education.html.twig */
class __TwigTemplate_0f93466eddd2ba041f5bb72279c1c4d6a25794b4e828b75208cfff67bf34f70b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- education section -->
<section id=\"";
        // line 2
        echo twig_escape_filter($this->env, ($context["slug"] ?? null), "html", null, true);
        echo "\" class=\"xs-onepage-section my-5\">
    <div class=\"container\">

        ";
        // line 5
        $this->loadTemplate("partials/sections/partials/title.html.twig", "partials/sections/education.html.twig", 5)->display($context);
        // line 6
        echo "
        <div id=\"education-items\" class=\"mt-3 mb-0 d-flex justify-content-center align-items-center\">
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 9
            echo "                <div class=\"d-flex justify-content-center align-items-center text-center p-3\">
                    <div class=\"education-box-container\">
                        <div class=\"education-box\">
                            <div class=\"education-image d-flex align-items-center justify-content-center\">
                                <img src=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "image", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
            echo "\">
                            </div>

                            <div class=\"thin-separator-line bg-secondary my-4\"></div>

                            <h4 class=\"university\">
                                ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
            echo "
                            </h4>

                            <div class=\"namerol\">
                                <span class=\"text-uppercase display-block black-text letter-spacing-2 margin-five no-margin-top\">
                                    ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "degree", array()), "html", null, true);
            echo "
                                </span>
                                <p>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "description", array()), "html", null, true);
            echo "</p>
                            </div>

                            <div class=\"thin-separator-line bg-secondary my-4\"></div>

                            <h5 class=\"year\">
                                ";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "years", array()), "html", null, true);
            echo "
                            </h5>
                            <p class=\"city text-muted\">
                                ";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "city", array()), "html", null, true);
            echo "
                            </p>
                        </div>

                    </div>
                </div>
                <!-- end education item -->
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "        </div>
    </div>
</section>
<!-- end education section -->
";
    }

    public function getTemplateName()
    {
        return "partials/sections/education.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 43,  83 => 35,  77 => 32,  68 => 26,  63 => 24,  55 => 19,  44 => 13,  38 => 9,  34 => 8,  30 => 6,  28 => 5,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- education section -->
<section id=\"{{ slug }}\" class=\"xs-onepage-section my-5\">
    <div class=\"container\">

        {% include 'partials/sections/partials/title.html.twig' %}

        <div id=\"education-items\" class=\"mt-3 mb-0 d-flex justify-content-center align-items-center\">
            {% for item in items %}
                <div class=\"d-flex justify-content-center align-items-center text-center p-3\">
                    <div class=\"education-box-container\">
                        <div class=\"education-box\">
                            <div class=\"education-image d-flex align-items-center justify-content-center\">
                                <img src=\"{{ item.image }}\" alt=\"{{ item.title }}\">
                            </div>

                            <div class=\"thin-separator-line bg-secondary my-4\"></div>

                            <h4 class=\"university\">
                                {{ item.title }}
                            </h4>

                            <div class=\"namerol\">
                                <span class=\"text-uppercase display-block black-text letter-spacing-2 margin-five no-margin-top\">
                                    {{ item.degree }}
                                </span>
                                <p>{{ item.description }}</p>
                            </div>

                            <div class=\"thin-separator-line bg-secondary my-4\"></div>

                            <h5 class=\"year\">
                                {{ item.years }}
                            </h5>
                            <p class=\"city text-muted\">
                                {{ item.city }}
                            </p>
                        </div>

                    </div>
                </div>
                <!-- end education item -->
            {% endfor %}
        </div>
    </div>
</section>
<!-- end education section -->
", "partials/sections/education.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\sections\\education.html.twig");
    }
}
