<?php

/* base.html.twig */
class __TwigTemplate_017562c3176c3eb5f006169e2f1e4ea0ef49b667d5037d467f834108abefd720 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'header' => array($this, 'block_header'),
            'main' => array($this, 'block_main'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- base.html.twig -->
<!doctype html>
<html class=\"no-js\" lang=\"en\">
";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 7
        echo "<link rel='stylesheet' href='dist/style.css'>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-KLRSS2C\"
            height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
";
        // line 15
        $this->displayBlock('header', $context, $blocks);
        // line 18
        $this->displayBlock('main', $context, $blocks);
        // line 19
        echo "
";
        // line 20
        $this->displayBlock('footer', $context, $blocks);
        // line 23
        echo "<script type=\"text/javascript\" src=\"dist/bundle.js\"></script>
</body>
</html>";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        $this->loadTemplate("partials/head.html.twig", "base.html.twig", 5)->display($context);
    }

    // line 15
    public function block_header($context, array $blocks = array())
    {
        // line 16
        echo "    ";
        $this->loadTemplate("partials/navigation.html.twig", "base.html.twig", 16)->display($context);
    }

    // line 18
    public function block_main($context, array $blocks = array())
    {
    }

    // line 20
    public function block_footer($context, array $blocks = array())
    {
        // line 21
        echo "    ";
        $this->loadTemplate("partials/footer.html.twig", "base.html.twig", 21)->display($context);
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  79 => 21,  76 => 20,  71 => 18,  66 => 16,  63 => 15,  58 => 5,  55 => 4,  49 => 23,  47 => 20,  44 => 19,  42 => 18,  40 => 15,  30 => 7,  28 => 4,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- base.html.twig -->
<!doctype html>
<html class=\"no-js\" lang=\"en\">
{% block head %}
    {% include 'partials/head.html.twig' %}
{% endblock %}
<link rel='stylesheet' href='dist/style.css'>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-KLRSS2C\"
            height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
{% block header %}
    {% include 'partials/navigation.html.twig' %}
{% endblock %}
{% block main %}{% endblock %}

{% block footer %}
    {% include 'partials/footer.html.twig' %}
{% endblock %}
<script type=\"text/javascript\" src=\"dist/bundle.js\"></script>
</body>
</html>", "base.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\base.html.twig");
    }
}
