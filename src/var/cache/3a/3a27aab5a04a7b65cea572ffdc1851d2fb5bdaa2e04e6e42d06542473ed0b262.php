<?php

/* partials/footer.html.twig */
class __TwigTemplate_400004f834d6c135a755c22c58cb95845626fe302544c087e4e34508e1a29ec0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- footer -->
<footer>
    <div class=\"footer-top py-5\">
        <div class=\"container\">
            <div class=\"row mb-4\">
                <!-- phone -->
                <div class=\"col-md-4 col-sm-4 text-center\">
                    <i class=\"icon-phone small-icon black-text\"></i>
                    <h6 class=\"black-text margin-two no-margin-bottom\">";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["sections"] ?? null), "about", array()), "phone", array()), "html", null, true);
        echo "</h6></div>
                <!-- end phone -->
                <!-- address -->
                <div class=\"col-md-4 col-sm-4 text-center\">
                    <i class=\"icon-map-pin small-icon black-text\"></i>
                    <h6 class=\"black-text margin-two no-margin-bottom\">";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["sections"] ?? null), "about", array()), "location", array()), "html", null, true);
        echo "</h6>
                </div>
                <!-- end address -->
                <!-- email -->
                <div class=\"col-md-4 col-sm-4 text-center\">
                    <i class=\"icon-envelope small-icon black-text\"></i>
                    <h6 class=\"margin-two no-margin-bottom\">
                        <a href=\"mailto:";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["sections"] ?? null), "about", array()), "email", array()), "html", null, true);
        echo "\" class=\"black-text\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["sections"] ?? null), "about", array()), "email", array()), "html", null, true);
        echo "</a>
                    </h6>
                </div>
                <!-- end email -->
            </div>
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 footer-social text-right text-center\">
                    <!-- social media link -->
                    ";
        // line 29
        if ($this->getAttribute($this->getAttribute(($context["page"] ?? null), "social_media", array(), "any", false, true), "facebook", array(), "any", true, true)) {
            // line 30
            echo "                        <a target=\"_blank\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "social_media", array()), "facebook", array()), "html", null, true);
            echo "\"><i class=\"fa fa-facebook\"></i></a>
                    ";
        }
        // line 32
        echo "                    ";
        if ($this->getAttribute($this->getAttribute(($context["page"] ?? null), "social_media", array()), "medium", array())) {
            // line 33
            echo "                        <a target=\"_blank\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "social_media", array()), "medium", array()), "html", null, true);
            echo "\"><i class=\"fa fa-medium\"></i></a>
                    ";
        }
        // line 35
        echo "                    ";
        if ($this->getAttribute($this->getAttribute(($context["page"] ?? null), "social_media", array()), "github", array())) {
            // line 36
            echo "                        <a target=\"_blank\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "social_media", array()), "github", array()), "html", null, true);
            echo "\"><i class=\"fa fa-github\"></i></a>
                    ";
        }
        // line 38
        echo "                    ";
        if ($this->getAttribute($this->getAttribute(($context["page"] ?? null), "social_media", array()), "xing", array())) {
            // line 39
            echo "                        <a target=\"_blank\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "social_media", array()), "xing", array()), "html", null, true);
            echo "\"><i class=\"fa fa-xing\"></i></a>
                    ";
        }
        // line 41
        echo "                    ";
        if ($this->getAttribute($this->getAttribute(($context["page"] ?? null), "social_media", array()), "linkedin", array())) {
            // line 42
            echo "                        <a target=\"_blank\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "social_media", array()), "linkedin", array()), "html", null, true);
            echo "\"><i class=\"fa fa-linkedin\"></i></a>
                    ";
        }
        // line 44
        echo "                    <!-- end social media link -->
                </div>
            </div>
        </div>
    </div>
    <div class=\"bg-dark footer-bottom py-3\">

        <ul class=\"nav justify-content-center\">
            <li class=\"nav-item copyright letter-spacing-1\">
                <a class=\"nav-link disabled\">";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "copyright", array()), "html", null, true);
        echo "</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"javascript:\$('#imprint.modal').modal('show');\">Imprint</a>
            </li>
        </ul>
    </div>
    <!-- scroll to top -->
    <a href=\"javascript:\" class=\"scrollToTop p-2\"><i class=\"fa fa-angle-up\"></i></a>
    <!-- scroll to top end... -->
</footer>
<!-- end footer -->

<!-- Imprint modal -->
";
        // line 67
        echo $this->getAttribute(($context["footer"] ?? null), "imprint", array());
        echo "

<!-- javascript libraries / javascript files set #2 -->
<script type=\"text/javascript\" src=\"js/jquery.min.js\"></script>
<script type=\"text/javascript\" src=\"js/modernizr.js\"></script>
<script type=\"text/javascript\" src=\"js/bootstrap.js\"></script>
<script type=\"text/javascript\" src=\"js/jquery.easing.1.3.js\"></script>
<script type=\"text/javascript\" src=\"js/skrollr.min.js\"></script>
<script type=\"text/javascript\" src=\"js/jquery.viewport.mini.js\"></script>
<script type=\"text/javascript\" src=\"js/smooth-scroll.js\"></script>
<!-- jquery appear -->
<script type=\"text/javascript\" src=\"js/jquery.appear.js\"></script>
<!-- animation -->
<script type=\"text/javascript\" src=\"js/wow.min.js\"></script>
<!-- page scroll -->
<script type=\"text/javascript\" src=\"js/page-scroll.js\"></script>
<!-- parallax -->
<script type=\"text/javascript\" src=\"js/jquery.parallax-1.1.3.js\"></script>
<!--portfolio with shorting tab -->
<!-- text effect  -->
<script type=\"text/javascript\" src=\"js/text-effect.js\"></script>
<!-- one page navigation -->
<script type=\"text/javascript\" src=\"js/one-page-main.js\"></script>
<!-- setting -->
<script type=\"text/javascript\" src=\"js/main.js\"></script>";
    }

    public function getTemplateName()
    {
        return "partials/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 67,  115 => 53,  104 => 44,  98 => 42,  95 => 41,  89 => 39,  86 => 38,  80 => 36,  77 => 35,  71 => 33,  68 => 32,  62 => 30,  60 => 29,  47 => 21,  37 => 14,  29 => 9,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- footer -->
<footer>
    <div class=\"footer-top py-5\">
        <div class=\"container\">
            <div class=\"row mb-4\">
                <!-- phone -->
                <div class=\"col-md-4 col-sm-4 text-center\">
                    <i class=\"icon-phone small-icon black-text\"></i>
                    <h6 class=\"black-text margin-two no-margin-bottom\">{{ sections.about.phone }}</h6></div>
                <!-- end phone -->
                <!-- address -->
                <div class=\"col-md-4 col-sm-4 text-center\">
                    <i class=\"icon-map-pin small-icon black-text\"></i>
                    <h6 class=\"black-text margin-two no-margin-bottom\">{{ sections.about.location }}</h6>
                </div>
                <!-- end address -->
                <!-- email -->
                <div class=\"col-md-4 col-sm-4 text-center\">
                    <i class=\"icon-envelope small-icon black-text\"></i>
                    <h6 class=\"margin-two no-margin-bottom\">
                        <a href=\"mailto:{{ sections.about.email }}\" class=\"black-text\">{{ sections.about.email }}</a>
                    </h6>
                </div>
                <!-- end email -->
            </div>
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 footer-social text-right text-center\">
                    <!-- social media link -->
                    {% if page.social_media.facebook is defined %}
                        <a target=\"_blank\" href=\"{{ page.social_media.facebook }}\"><i class=\"fa fa-facebook\"></i></a>
                    {% endif %}
                    {% if page.social_media.medium %}
                        <a target=\"_blank\" href=\"{{ page.social_media.medium }}\"><i class=\"fa fa-medium\"></i></a>
                    {% endif %}
                    {% if page.social_media.github %}
                        <a target=\"_blank\" href=\"{{ page.social_media.github }}\"><i class=\"fa fa-github\"></i></a>
                    {% endif %}
                    {% if page.social_media.xing %}
                        <a target=\"_blank\" href=\"{{ page.social_media.xing }}\"><i class=\"fa fa-xing\"></i></a>
                    {% endif %}
                    {% if page.social_media.linkedin %}
                        <a target=\"_blank\" href=\"{{ page.social_media.linkedin }}\"><i class=\"fa fa-linkedin\"></i></a>
                    {% endif %}
                    <!-- end social media link -->
                </div>
            </div>
        </div>
    </div>
    <div class=\"bg-dark footer-bottom py-3\">

        <ul class=\"nav justify-content-center\">
            <li class=\"nav-item copyright letter-spacing-1\">
                <a class=\"nav-link disabled\">{{ page.copyright }}</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"javascript:\$('#imprint.modal').modal('show');\">Imprint</a>
            </li>
        </ul>
    </div>
    <!-- scroll to top -->
    <a href=\"javascript:\" class=\"scrollToTop p-2\"><i class=\"fa fa-angle-up\"></i></a>
    <!-- scroll to top end... -->
</footer>
<!-- end footer -->

<!-- Imprint modal -->
{{ footer.imprint | raw }}

<!-- javascript libraries / javascript files set #2 -->
<script type=\"text/javascript\" src=\"js/jquery.min.js\"></script>
<script type=\"text/javascript\" src=\"js/modernizr.js\"></script>
<script type=\"text/javascript\" src=\"js/bootstrap.js\"></script>
<script type=\"text/javascript\" src=\"js/jquery.easing.1.3.js\"></script>
<script type=\"text/javascript\" src=\"js/skrollr.min.js\"></script>
<script type=\"text/javascript\" src=\"js/jquery.viewport.mini.js\"></script>
<script type=\"text/javascript\" src=\"js/smooth-scroll.js\"></script>
<!-- jquery appear -->
<script type=\"text/javascript\" src=\"js/jquery.appear.js\"></script>
<!-- animation -->
<script type=\"text/javascript\" src=\"js/wow.min.js\"></script>
<!-- page scroll -->
<script type=\"text/javascript\" src=\"js/page-scroll.js\"></script>
<!-- parallax -->
<script type=\"text/javascript\" src=\"js/jquery.parallax-1.1.3.js\"></script>
<!--portfolio with shorting tab -->
<!-- text effect  -->
<script type=\"text/javascript\" src=\"js/text-effect.js\"></script>
<!-- one page navigation -->
<script type=\"text/javascript\" src=\"js/one-page-main.js\"></script>
<!-- setting -->
<script type=\"text/javascript\" src=\"js/main.js\"></script>", "partials/footer.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\footer.html.twig");
    }
}
