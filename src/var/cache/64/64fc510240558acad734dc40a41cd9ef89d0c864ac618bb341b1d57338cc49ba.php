<?php

/* partials/head.html.twig */
class __TwigTemplate_eae9e365e9d3141b6a8badc567ec286b6d37d7017b7d6d30556f921f48fd9c33 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<head>
    <title>";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "title", array()), "html", null, true);
        echo "</title>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start'                    :
                    new Date().getTime(), event: 'gtm.js'
            });
            var f                          = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KLRSS2C');</script>
    <!-- End Google Tag Manager -->
    <meta name=\"description\" content=\"Sebastian Buckpesch - CTO and lead developer\">
    <meta name=\"keywords\" content=\"\">
    <meta charset=\"utf-8\">
    <meta name=\"author\" content=\"Sebastian Buckpesch\">
    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0,maximum-scale=1\" />
    <!-- favicon -->
    <link rel=\"shortcut icon\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "favicon", array()), "html", null, true);
        echo "\">
    <link rel=\"apple-touch-icon\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "favicon", array()), "html", null, true);
        echo "\">
    <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "favicon", array()), "html", null, true);
        echo "\">
    <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "favicon", array()), "html", null, true);
        echo "\">
    <!-- animation -->
    <link rel=\"stylesheet\" href=\"css/animate.css\" />
    <!-- bootstrap -->
    <!--<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css\"
          integrity=\"sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb\" crossorigin=\"anonymous\">
    <!--<link rel=\"stylesheet\" href=\"css/bootstrap.css\" />-->
    <!-- et line icon -->
    <link rel=\"stylesheet\" href=\"css/et-line-icons.css\" />
    <!-- revolution slider -->
    <link rel=\"stylesheet\" href=\"css/extralayers.css\" />
    <link rel=\"stylesheet\" href=\"css/settings.css\" />
    <!-- magnific popup -->
    <link rel=\"stylesheet\" href=\"css/magnific-popup.css\" />
    <!-- owl carousel -->
    <link rel=\"stylesheet\" href=\"css/owl.carousel.css\" />
    <link rel=\"stylesheet\" href=\"css/owl.transitions.css\" />
    <link rel=\"stylesheet\" href=\"css/full-slider.css\" />
    <!-- text animation -->
    <link rel=\"stylesheet\" href=\"css/text-effect.css\" />
    <!-- hamburger menu  -->
    <link rel=\"stylesheet\" href=\"css/menu-hamburger.css\" />

    <!-- responsive -->
    <link rel=\"stylesheet\" href=\"css/responsive.css\" />
    <!--[if IE]>
    <link rel=\"stylesheet\" href=\"css/style-ie.css\" />
    <![endif]-->
    <!--[if IE]>
    <script src=\"js/html5shiv.js\"></script>
    <![endif]-->

    <!-- Custom styling -->
    <link rel=\"stylesheet\" href=\"dist/style.css\"/>
    <!-- common -->
    <link rel=\"stylesheet\" href=\"css/style.css\" />
</head>";
    }

    public function getTemplateName()
    {
        return "partials/head.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 27,  55 => 26,  51 => 25,  47 => 24,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<head>
    <title>{{ page.title }}</title>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start'                    :
                    new Date().getTime(), event: 'gtm.js'
            });
            var f                          = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KLRSS2C');</script>
    <!-- End Google Tag Manager -->
    <meta name=\"description\" content=\"Sebastian Buckpesch - CTO and lead developer\">
    <meta name=\"keywords\" content=\"\">
    <meta charset=\"utf-8\">
    <meta name=\"author\" content=\"Sebastian Buckpesch\">
    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0,maximum-scale=1\" />
    <!-- favicon -->
    <link rel=\"shortcut icon\" href=\"{{ page.favicon }}\">
    <link rel=\"apple-touch-icon\" href=\"{{ page.favicon }}\">
    <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"{{ page.favicon }}\">
    <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"{{ page.favicon }}\">
    <!-- animation -->
    <link rel=\"stylesheet\" href=\"css/animate.css\" />
    <!-- bootstrap -->
    <!--<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css\"
          integrity=\"sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb\" crossorigin=\"anonymous\">
    <!--<link rel=\"stylesheet\" href=\"css/bootstrap.css\" />-->
    <!-- et line icon -->
    <link rel=\"stylesheet\" href=\"css/et-line-icons.css\" />
    <!-- revolution slider -->
    <link rel=\"stylesheet\" href=\"css/extralayers.css\" />
    <link rel=\"stylesheet\" href=\"css/settings.css\" />
    <!-- magnific popup -->
    <link rel=\"stylesheet\" href=\"css/magnific-popup.css\" />
    <!-- owl carousel -->
    <link rel=\"stylesheet\" href=\"css/owl.carousel.css\" />
    <link rel=\"stylesheet\" href=\"css/owl.transitions.css\" />
    <link rel=\"stylesheet\" href=\"css/full-slider.css\" />
    <!-- text animation -->
    <link rel=\"stylesheet\" href=\"css/text-effect.css\" />
    <!-- hamburger menu  -->
    <link rel=\"stylesheet\" href=\"css/menu-hamburger.css\" />

    <!-- responsive -->
    <link rel=\"stylesheet\" href=\"css/responsive.css\" />
    <!--[if IE]>
    <link rel=\"stylesheet\" href=\"css/style-ie.css\" />
    <![endif]-->
    <!--[if IE]>
    <script src=\"js/html5shiv.js\"></script>
    <![endif]-->

    <!-- Custom styling -->
    <link rel=\"stylesheet\" href=\"dist/style.css\"/>
    <!-- common -->
    <link rel=\"stylesheet\" href=\"css/style.css\" />
</head>", "partials/head.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\head.html.twig");
    }
}
