<?php

/* partials/sections/contact.html.twig */
class __TwigTemplate_b3869273690692118712933f7ce4b6ec9f74b53d450856a7ad94767da34a2433 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- contact us section -->
<section id=\"";
        // line 2
        echo twig_escape_filter($this->env, ($context["slug"] ?? null), "html", null, true);
        echo "\" class=\"xs-onepage-section\">
    <div class=\"container\">

        ";
        // line 5
        $this->loadTemplate("partials/sections/partials/title.html.twig", "partials/sections/contact.html.twig", 5)->display($context);
        // line 6
        echo "
        <div class=\"row mt-5 mb-0\">
            <div class=\"col-sm-4 text-center\">
                <i class=\"icon-phone medium-icon black-text\"></i>
                <h6 class=\"black-text margin-two no-margin-bottom\">";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["sections"] ?? null), "about", array()), "phone", array()), "html", null, true);
        echo "</h6>
            </div>
            <div class=\"col-sm-4 text-center\">
                <i class=\"icon-map-pin medium-icon black-text\"></i>
                <h6 class=\"black-text margin-two no-margin-bottom\">";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["sections"] ?? null), "about", array()), "location", array()), "html", null, true);
        echo "</h6>
            </div>
            <div class=\"col-sm-4 text-center\">
                <i class=\"icon-envelope medium-icon black-text\"></i>
                <h6 class=\"black-text margin-two no-margin-bottom\">
                    <a class=\"black-text\" href=\"mailto:";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["sections"] ?? null), "about", array()), "email", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["sections"] ?? null), "about", array()), "email", array()), "html", null, true);
        echo "</a>
                </h6>
            </div>
        </div>
        <!-- form -->
        ";
        // line 54
        echo "        <!-- end form -->
    </div>
</section>
<!-- end contact us section -->";
    }

    public function getTemplateName()
    {
        return "partials/sections/contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 54,  51 => 19,  43 => 14,  36 => 10,  30 => 6,  28 => 5,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- contact us section -->
<section id=\"{{ slug }}\" class=\"xs-onepage-section\">
    <div class=\"container\">

        {% include 'partials/sections/partials/title.html.twig' %}

        <div class=\"row mt-5 mb-0\">
            <div class=\"col-sm-4 text-center\">
                <i class=\"icon-phone medium-icon black-text\"></i>
                <h6 class=\"black-text margin-two no-margin-bottom\">{{ sections.about.phone }}</h6>
            </div>
            <div class=\"col-sm-4 text-center\">
                <i class=\"icon-map-pin medium-icon black-text\"></i>
                <h6 class=\"black-text margin-two no-margin-bottom\">{{ sections.about.location }}</h6>
            </div>
            <div class=\"col-sm-4 text-center\">
                <i class=\"icon-envelope medium-icon black-text\"></i>
                <h6 class=\"black-text margin-two no-margin-bottom\">
                    <a class=\"black-text\" href=\"mailto:{{ sections.about.email }}\">{{ sections.about.email }}</a>
                </h6>
            </div>
        </div>
        <!-- form -->
        {#<form id=\"contactusform\" action=\"javascript:void(0)\" method=\"post\">
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12\">
                    <div class=\"wide-separator-line my-4\"></div>
                </div>
                <div class=\"col-md-12 col-sm-12\">
                    <div id=\"success\"></div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-6 col-sm-12\">
                    <input name=\"name\" type=\"text\" placeholder=\"Your Name\"/>
                    <input name=\"email\" type=\"text\" placeholder=\"Your Email\"/>
                </div>
                <div class=\"col-md-6 col-sm-12\">
                    <textarea name=\"comment\" placeholder=\"Your Message\"></textarea>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-6 col-sm-6\">
                    <span class=\"required\">*Please complete all fields correctly</span>
                </div>
                <div class=\"col-md-3 col-sm-6\">
                    <input id=\"contact-us-button\"
                           name=\"send message\" type=\"button\"
                           value=\"send message\"
                           class=\"btn btn-black no-margin-top f-right no-margin-lr\">
                </div>
            </div>
        </form>#}
        <!-- end form -->
    </div>
</section>
<!-- end contact us section -->", "partials/sections/contact.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\sections\\contact.html.twig");
    }
}
