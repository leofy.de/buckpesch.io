<?php

/* partials/sections/header.html.twig */
class __TwigTemplate_bfa2a7c885e3b4caeaf0d1f7d5d28b5af3909ede93aca53cf7a42723a5e83f2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- head section -->
<section id=\"";
        // line 2
        echo twig_escape_filter($this->env, ($context["slug"] ?? null), "html", null, true);
        echo "\" class=\"parallax1 parallax-fix full-screen p-0 scroll-to-down\"
         onclick=\"scrollToDown('";
        // line 3
        echo twig_escape_filter($this->env, ($context["scrollTo"] ?? null), "html", null, true);
        echo "')\">
    <div class=\"overlay-gray\"></div>
    <img class=\"parallax-background-img\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, ($context["background"] ?? null), "html", null, true);
        echo "\" alt=\"\"/>
    <div class=\"container full-screen position-relative\">
        <div class=\"slider-typography\">
            <div class=\"slider-text-middle-main\">
                <div class=\"slider-text-middle slider-text-middle2 personal-name animated fadeIn\">
                    <h1 class=\"my-2 text-uppercase \">";
        // line 10
        echo twig_escape_filter($this->env, ($context["headline"] ?? null), "html", null, true);
        echo "</h1>
                    <span class=\"cd-headline letters type text-uppercase\">
                        <span class=\"cd-words-wrapper waiting\">
                            ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["slogans"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["slogan"]) {
            // line 14
            echo "                                ";
            if ($this->getAttribute($context["loop"], "first", array())) {
                // line 15
                echo "                                    <b class=\"is-visible main-font text-large font-weight-400\">";
                echo twig_escape_filter($this->env, $context["slogan"], "html", null, true);
                echo "</b>
                                ";
            } else {
                // line 17
                echo "                                    <b class=\"main-font text-large font-weight-400\">";
                echo twig_escape_filter($this->env, $context["slogan"], "html", null, true);
                echo "</b>
                                ";
            }
            // line 19
            echo "                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slogan'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "                        </span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end head section -->";
    }

    public function getTemplateName()
    {
        return "partials/sections/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 20,  77 => 19,  71 => 17,  65 => 15,  62 => 14,  45 => 13,  39 => 10,  31 => 5,  26 => 3,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- head section -->
<section id=\"{{ slug }}\" class=\"parallax1 parallax-fix full-screen p-0 scroll-to-down\"
         onclick=\"scrollToDown('{{ scrollTo }}')\">
    <div class=\"overlay-gray\"></div>
    <img class=\"parallax-background-img\" src=\"{{ background }}\" alt=\"\"/>
    <div class=\"container full-screen position-relative\">
        <div class=\"slider-typography\">
            <div class=\"slider-text-middle-main\">
                <div class=\"slider-text-middle slider-text-middle2 personal-name animated fadeIn\">
                    <h1 class=\"my-2 text-uppercase \">{{ headline }}</h1>
                    <span class=\"cd-headline letters type text-uppercase\">
                        <span class=\"cd-words-wrapper waiting\">
                            {% for slogan in slogans %}
                                {% if loop.first %}
                                    <b class=\"is-visible main-font text-large font-weight-400\">{{ slogan }}</b>
                                {% else %}
                                    <b class=\"main-font text-large font-weight-400\">{{ slogan }}</b>
                                {% endif %}
                            {% endfor %}
                        </span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end head section -->", "partials/sections/header.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\sections\\header.html.twig");
    }
}
