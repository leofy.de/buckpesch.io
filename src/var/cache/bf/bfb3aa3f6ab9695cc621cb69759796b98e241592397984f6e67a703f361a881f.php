<?php

/* partials/sections/skills.html.twig */
class __TwigTemplate_e655b0cf73ee68acef0b78bfda7e005ff6e3511930cf0a6929ebbbcdba6ec86a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- skills section -->
<section id=\"";
        // line 2
        echo twig_escape_filter($this->env, ($context["slug"] ?? null), "html", null, true);
        echo "\" class=\"xs-onepage-section\">
    <div class=\"container\">

        ";
        // line 5
        $this->loadTemplate("partials/sections/partials/title.html.twig", "partials/sections/skills.html.twig", 5)->display($context);
        // line 6
        echo "
        <div class=\"row mt-5 mb-0\">
            <!-- features grid -->
            ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
            // line 10
            echo "                <!-- features item -->
                <div class=\"col-lg-4 col-md-6 col-12\">
                    <div class=\"row mb-5\">
                        <div class=\"col-2 col-sm-3\">
                            <i class=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["skill"], "icon", array()), "html", null, true);
            echo " medium-icon \"></i>
                        </div>
                        <div class=\"col-10 col-sm-9 text-left\">
                            <h5 class=\"margin-five no-margin-top\">";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["skill"], "title", array()), "html", null, true);
            echo "
                                ";
            // line 22
            echo "                            </h5>
                            <p class=\"width-90\">";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["skill"], "description", array()), "html", null, true);
            echo "</p>
                        </div>
                    </div>
                </div>
                <!-- end features item -->
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        </div>
    </div>
</section>
<!-- end skills section -->";
    }

    public function getTemplateName()
    {
        return "partials/sections/skills.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 29,  58 => 23,  55 => 22,  51 => 17,  45 => 14,  39 => 10,  35 => 9,  30 => 6,  28 => 5,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- skills section -->
<section id=\"{{ slug }}\" class=\"xs-onepage-section\">
    <div class=\"container\">

        {% include 'partials/sections/partials/title.html.twig' %}

        <div class=\"row mt-5 mb-0\">
            <!-- features grid -->
            {% for skill in items %}
                <!-- features item -->
                <div class=\"col-lg-4 col-md-6 col-12\">
                    <div class=\"row mb-5\">
                        <div class=\"col-2 col-sm-3\">
                            <i class=\"{{ skill.icon }} medium-icon \"></i>
                        </div>
                        <div class=\"col-10 col-sm-9 text-left\">
                            <h5 class=\"margin-five no-margin-top\">{{ skill.title }}
                                {#<div class=\"gps_ring\"
                                     data-toggle=\"tooltip\"
                                     data-placement=\"top\"
                                     title=\"Show what's inside...\"></div>#}
                            </h5>
                            <p class=\"width-90\">{{ skill.description }}</p>
                        </div>
                    </div>
                </div>
                <!-- end features item -->
            {% endfor %}
        </div>
    </div>
</section>
<!-- end skills section -->", "partials/sections/skills.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\sections\\skills.html.twig");
    }
}
