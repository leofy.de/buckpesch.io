<?php

/* partials/components/modal.html.twig */
class __TwigTemplate_46c0561a8c5c950d4061f290a3b986ff09d062d2b316678b4a32ee8d6a0bc484 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" class=\"modal\" tabindex=\"-1\" role=\"dialog\">
    <div class=\"modal-dialog modal-lg\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\">";
        // line 5
        echo ($context["title"] ?? null);
        echo "</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                ";
        // line 11
        echo ($context["body"] ?? null);
        echo "
            </div>
            ";
        // line 13
        if (($context["footer"] ?? null)) {
            // line 14
            echo "            <div class=\"modal-footer\">
                ";
            // line 15
            echo ($context["footer"] ?? null);
            echo "
            </div>
            ";
        }
        // line 18
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "partials/components/modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 18,  46 => 15,  43 => 14,  41 => 13,  36 => 11,  27 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"{{ id }}\" class=\"modal\" tabindex=\"-1\" role=\"dialog\">
    <div class=\"modal-dialog modal-lg\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\">{{ title | raw }}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                {{ body | raw }}
            </div>
            {% if footer %}
            <div class=\"modal-footer\">
                {{ footer | raw }}
            </div>
            {% endif %}
        </div>
    </div>
</div>", "partials/components/modal.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\components\\modal.html.twig");
    }
}
