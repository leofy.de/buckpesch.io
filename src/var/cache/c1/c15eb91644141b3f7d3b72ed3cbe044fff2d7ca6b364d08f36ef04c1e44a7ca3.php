<?php

/* partials/sections/work.html.twig */
class __TwigTemplate_17bb74d0a69cf571da0a9ec261c572073a82a28ab8afa9e11eccf5f87ac9ba36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- work section -->
<section id=\"";
        // line 2
        echo twig_escape_filter($this->env, ($context["slug"] ?? null), "html", null, true);
        echo "\" class=\"xs-onepage-section my-5\">
    <div class=\"container\">

        ";
        // line 5
        $this->loadTemplate("partials/sections/partials/title.html.twig", "partials/sections/work.html.twig", 5)->display($context);
        // line 6
        echo "
        <div id=\"work-items\" class=\"mt-3 mb-0 d-flex justify-content-center align-items-center\">
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 9
            echo "                <div class=\"d-flex justify-content-center align-items-center text-center p-3 \">
                    <div class=\"work-box-container\">
                        <div class=\"work-box\">
                            <div class=\"work-image d-flex align-items-center justify-content-center\">
                                <img src=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "image", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
            echo "\">
                            </div>

                            <div class=\"thin-separator-line bg-secondary my-4\"></div>

                            <h4 class=\"work-company my-2\">
                                ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
            echo "
                            </h4>
                            <div class=\"namerol\">
                                <span class=\"text-uppercase display-block black-text letter-spacing-2\">
                                    ";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "position", array()), "html", null, true);
            echo "
                                </span>
                                <p>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "description", array()), "html", null, true);
            echo "</p>
                            </div>

                            <div class=\"thin-separator-line bg-secondary my-4\"></div>

                            <h5 class=\"year\">
                                ";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "years", array()), "html", null, true);
            echo "
                            </h5>
                            <p class=\"city text-muted\">
                                ";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "city", array()), "html", null, true);
            echo "
                            </p>
                        </div>

                    </div>
                </div>
                <!-- end work item -->
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "        </div>
    </div>
</section>
<!-- end work section -->
";
    }

    public function getTemplateName()
    {
        return "partials/sections/work.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 42,  82 => 34,  76 => 31,  67 => 25,  62 => 23,  55 => 19,  44 => 13,  38 => 9,  34 => 8,  30 => 6,  28 => 5,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- work section -->
<section id=\"{{ slug }}\" class=\"xs-onepage-section my-5\">
    <div class=\"container\">

        {% include 'partials/sections/partials/title.html.twig' %}

        <div id=\"work-items\" class=\"mt-3 mb-0 d-flex justify-content-center align-items-center\">
            {% for item in items %}
                <div class=\"d-flex justify-content-center align-items-center text-center p-3 \">
                    <div class=\"work-box-container\">
                        <div class=\"work-box\">
                            <div class=\"work-image d-flex align-items-center justify-content-center\">
                                <img src=\"{{ item.image }}\" alt=\"{{ item.title }}\">
                            </div>

                            <div class=\"thin-separator-line bg-secondary my-4\"></div>

                            <h4 class=\"work-company my-2\">
                                {{ item.title }}
                            </h4>
                            <div class=\"namerol\">
                                <span class=\"text-uppercase display-block black-text letter-spacing-2\">
                                    {{ item.position }}
                                </span>
                                <p>{{ item.description }}</p>
                            </div>

                            <div class=\"thin-separator-line bg-secondary my-4\"></div>

                            <h5 class=\"year\">
                                {{ item.years }}
                            </h5>
                            <p class=\"city text-muted\">
                                {{ item.city }}
                            </p>
                        </div>

                    </div>
                </div>
                <!-- end work item -->
            {% endfor %}
        </div>
    </div>
</section>
<!-- end work section -->
", "partials/sections/work.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\sections\\work.html.twig");
    }
}
