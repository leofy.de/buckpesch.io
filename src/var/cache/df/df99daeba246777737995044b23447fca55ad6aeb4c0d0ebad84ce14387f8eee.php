<?php

/* partials/navigation.html.twig */
class __TwigTemplate_79867b05df18eb3d18af54986a93bd2e5932c77b75111014f76b2629e2c7b7a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- navigation -->
<nav class=\"navbar navbar-expand-lg fixed-top navbar-light nav-transparent overlay-nav sticky-nav nav-border-bottom no-transition\"
     role=\"navigation\">
    <a class=\"navbar-brand logo-light\" href=\"/\">
        <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "logo", array()), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "title", array()), "html", null, true);
        echo " Logo\">
    </a>
    <a class=\"navbar-brand logo-dark\" href=\"/\">
        <img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "logo", array()), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "title", array()), "html", null, true);
        echo " Logo\">
    </a>

    <button type=\"button\" class=\"navbar-toggler ml-auto\" data-toggle=\"collapse\" data-target=\"#mainNav\">
        <i class=\"icon-layers\"></i>
    </button>
    <div id=\"mainNav\" class=\"navbar-collapse collapse mb-2\">
        <ul class=\"navbar-nav ml-auto\">
            ";
        // line 17
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sections"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
            // line 18
            echo "                ";
            if ($this->getAttribute($context["section"], "slug", array())) {
                // line 19
                echo "                    <li class=\"nav-item\"><a href=\"#";
                echo twig_escape_filter($this->env, $this->getAttribute($context["section"], "slug", array()), "html", null, true);
                echo "\"
                                            class=\"nav-link inner-link\">";
                // line 20
                echo twig_escape_filter($this->env, $this->getAttribute($context["section"], "title", array()), "html", null, true);
                echo "</a></li>
                ";
            }
            // line 22
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "            <!-- Languages selector -->
            ";
        // line 24
        if ($this->getAttribute(($context["navigation"] ?? null), "languages", array())) {
            // line 25
            echo "                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"languageSelectorDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        <img src=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["navigation"] ?? null), "languageCurrent", array()), "image", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["navigation"] ?? null), "languageCurrent", array()), "title", array()), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["navigation"] ?? null), "languageCurrent", array()), "title", array()), "html", null, true);
            echo "\">
                    </a>
                    <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"languageSelectorDropdown\">
                        ";
            // line 30
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["navigation"] ?? null), "languages", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 31
                echo "                            ";
                if (($this->getAttribute($context["language"], "current", array()) == false)) {
                    // line 32
                    echo "                                <a class=\"dropdown-item\" href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["language"], "url", array()), "html", null, true);
                    echo "\">
                                    <img src=\"";
                    // line 33
                    echo twig_escape_filter($this->env, $this->getAttribute($context["language"], "image", array()), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["language"], "title", array()), "html", null, true);
                    echo "\"> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["language"], "title", array()), "html", null, true);
                    echo "
                                </a>
                            ";
                }
                // line 36
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "                    </div>
                </li>
            ";
        }
        // line 40
        echo "
        </ul>
    </div>
</nav>
<!-- end of navigation -->";
    }

    public function getTemplateName()
    {
        return "partials/navigation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 40,  117 => 37,  111 => 36,  101 => 33,  96 => 32,  93 => 31,  89 => 30,  79 => 27,  75 => 25,  73 => 24,  70 => 23,  64 => 22,  59 => 20,  54 => 19,  51 => 18,  46 => 17,  33 => 8,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- navigation -->
<nav class=\"navbar navbar-expand-lg fixed-top navbar-light nav-transparent overlay-nav sticky-nav nav-border-bottom no-transition\"
     role=\"navigation\">
    <a class=\"navbar-brand logo-light\" href=\"/\">
        <img src=\"{{ page.logo }}\" title=\"{{ page.title }} Logo\">
    </a>
    <a class=\"navbar-brand logo-dark\" href=\"/\">
        <img src=\"{{ page.logo }}\" title=\"{{ page.title }} Logo\">
    </a>

    <button type=\"button\" class=\"navbar-toggler ml-auto\" data-toggle=\"collapse\" data-target=\"#mainNav\">
        <i class=\"icon-layers\"></i>
    </button>
    <div id=\"mainNav\" class=\"navbar-collapse collapse mb-2\">
        <ul class=\"navbar-nav ml-auto\">
            {# Include all sections in order they have been passed #}
            {% for section in sections %}
                {% if section.slug %}
                    <li class=\"nav-item\"><a href=\"#{{ section.slug }}\"
                                            class=\"nav-link inner-link\">{{ section.title }}</a></li>
                {% endif %}
            {% endfor %}
            <!-- Languages selector -->
            {% if navigation.languages %}
                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"languageSelectorDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        <img src=\"{{ navigation.languageCurrent.image }}\" alt=\"{{ navigation.languageCurrent.title }}\" title=\"{{ navigation.languageCurrent.title }}\">
                    </a>
                    <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"languageSelectorDropdown\">
                        {% for language in navigation.languages %}
                            {% if language.current == false %}
                                <a class=\"dropdown-item\" href=\"{{ language.url }}\">
                                    <img src=\"{{ language.image }}\" alt=\"{{ language.title }}\"> {{ language.title }}
                                </a>
                            {% endif %}
                        {% endfor %}
                    </div>
                </li>
            {% endif %}

        </ul>
    </div>
</nav>
<!-- end of navigation -->", "partials/navigation.html.twig", "C:\\Users\\sbuck\\projects\\buckpesch.io\\source\\view\\partials\\navigation.html.twig");
    }
}
