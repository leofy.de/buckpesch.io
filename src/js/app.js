// Import JS
import $ from 'jquery';
import 'slick-carousel';
import 'bootstrap';
// Import SCSS
import '../css/scss/index.scss';

// Slider slider for education items
$(function () {

    const slickConfig = {
        adaptiveHeight: true,
        dots          : true,
        infinite      : false,
        speed         : 300,
        slidesToShow  : 4,
        slidesToScroll: 4,
        responsive    : [
            {
                breakpoint: 1024,
                settings  : {
                    slidesToShow  : 3,
                    slidesToScroll: 3,
                    infinite      : true,
                    dots          : true
                }
            },
            {
                breakpoint: 800,
                settings  : {
                    slidesToShow  : 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings  : {
                    slidesToShow  : 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    };

    // Add tooltips for
    $('[data-toggle="tooltip"]').tooltip();

    // Initialize sliders
    let educationSlider = $('#education-items');
    let workSlider      = $('#work-items');
    educationSlider.slick(slickConfig);
    workSlider.slick(slickConfig);

});