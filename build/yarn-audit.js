const fs = require("fs");
const filepath = "./reports/yarn-audit.json";

try {
    const report = fs
        .readFileSync(filepath, "utf8")
        .toString()
        .split("\n");
    const packageJson = require("../package.json");
    const advisoryURL = "https://npmjs.com/advisories/";
    const advisories = report
        .map(item => {
            try {
                return JSON.parse(item);
            } catch (e) {
                return null;
            }
        })
        .filter(advisory => advisory !== null && advisory.type === "auditAdvisory");

    const findings = advisories.filter(advisory => {
        // Check for all findings if the root module is in devDependencies
        const advisoryFindings = advisory.data.advisory.findings;
        const advisoryFindingsProduction = advisoryFindings.filter(find => {
            const rootModule = find.paths[0].split(">")[0];
            return Object.keys(packageJson.dependencies).includes(rootModule);
        });

        return advisoryFindingsProduction.length > 0;
    });

    if (findings.length > 0) {
        console.log(`found ${findings.length} vulnerabilities among production dependencies. Please visit below link for details`);
        console.log("--------------------");
        findings.forEach(finding => {
            console.log(`URL: ${advisoryURL}${finding.data.resolution.id}`);
            console.log(`Path: ${finding.data.resolution.path}`);
            console.log("--------------------");
        });
        try {
            fs.unlinkSync(filepath);
        } catch (err) {
            console.error(err);
        }
        process.exit(1);
    } else {
        try {
            fs.unlinkSync(filepath);
        } catch (err) {
            console.error(err);
        }
        process.exit(0);
    }
} catch (e) {
    console.log(e);
    try {
        fs.unlinkSync(filepath);
    } catch (err) {
        console.error(err);
    }
    process.exit(1);
}
