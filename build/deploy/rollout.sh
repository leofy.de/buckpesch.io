#!/bin/bash

# Validate input parameters
SUPPORTED_ENVS=(prod preprod)
if [[ ! " ${SUPPORTED_ENVS[@]} " =~ " ${ENV} " ]]; then
  echo ""
  echo "=> Missing parameter: ENV (allowed values: ${SUPPORTED_ENVS[@]})"
  echo ""
  exit 1
fi

# Validate TARGET_PATH
if [ -z "$TARGET_PATH" ]; then
  echo ""
  echo "=> Missing parameter: TARGET_PATH or artifact.zip does not exist in TARGET_PATH"
  echo ""
  exit 1
fi

# Validate if artifact.zip exists in target path
if [ ! -f "$TARGET_PATH$ENV.next/artifact.zip" ]; then
  echo ""
  echo "=> Missing artifact: $TARGET_PATH$ENV.next/artifact.zip does not exist"
  echo ""
  exit 1
fi

echo ""
echo "=> Rolling out new version to: $TARGET_PATH$ENV/"
echo ""

# Unpack artifact in target path
echo ""
echo "Unpacking artifact"
echo ""
cd "$TARGET_PATH$ENV".next || exit
unzip -q artifact.zip || exit
rm artifact.zip

echo ""
echo "Copying config files"
echo ""
cd "$TARGET_PATH" || exit
cp -R config/"${ENV}"/. "${ENV}".next/ || exit

echo ""
echo "Setting access rights"
echo ""
cd "$TARGET_PATH" || exit
chmod -R 775 "${ENV}".next/.
find "${ENV}".next/ -type d -exec chmod 775 {} \;
find "${ENV}".next/ -type f -exec chmod 664 {} \;
chown -R www-data:www-data "${ENV}".next/.

echo ""
echo "Rename folders"
echo ""
cd "$TARGET_PATH" || exit
# Rename folders
rm -R "${ENV}".prev/
mv "${ENV}"/ "${ENV}".prev/
mv "${ENV}".next/ "${ENV}"/
