const webpack = require('webpack');
const path = require('path');
const config = require('./config');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './src/js/app.js',
    output: {
        path: config.paths.dist,
        filename: 'bundle.js'
    },
    //watch: true, //live-reloading
    //devtool: 'source-map'
    module: {
        rules: [{
            test: /\.scss$/,
            include: config.paths.assets,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader',
                    options: {
                        modules: true,
                        //localIdentName: '[name]-[local]-[hash:base64]',
                        localIdentName: '[local]',
                    }
                },
                    //'css-loader?module',
                    'resolve-url-loader',
                    'sass-loader?sourceMap'
                ]
            })
        },
            {
                test: /\.(ttf|eot|woff2?|png|jpe?g|gif|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: `vendor/[name].[ext]`
                    }
                }]
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin('style.css'),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
            // In case you imported plugins individually, you must also require them here:
            //Util           : "exports-loader?Tooltip!bootstrap/js/dist/util",
            //Tooltip        : "exports-loader?Tooltip!bootstrap/js/dist/tooltip",
        })
    ]
};